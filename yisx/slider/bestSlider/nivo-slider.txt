nivo-slider最佳实践：
1,使用jquery的data方法记录插件的配置参数，并且可以使用层级关键字，值也可以是对象，如下：$("div")("nivo:vars",{key:"value"});
2,有些元素是没有width属性的。对没有width属性的元素设置width属性，将不会影响元素的宽度。
3,对多个变量可以挂靠在某一个对象vars上。
4,对元素绑定事件推荐使用on方法，$fatherDiv.on(event_name,selector,data,callback)。
5,使用自定义事件，绑定回调函数，同样可以通过trigger触发。
6,暴露默认配置。
7,使用私有状态变量，所谓的状态机。

